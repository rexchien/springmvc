package com.solvento.mvc.spring.controller;

import com.solvento.mvc.spring.bean.CountryBean;
import com.solvento.mvc.spring.persistence.Country;
import com.solvento.mvc.spring.service.CountryDaoService;
import com.sun.javafx.sg.prism.NGShape;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>Title: HomeController</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 上午11:49</p>
 *
 * @author Rex Chien
 * @version 1.0
 */

@Controller
@RequestMapping("/country")
public class CountryController {
	@Resource
	private CountryDaoService countryDaoService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView listView(ModelAndView mav) {
		mav.addObject("countries", countryDaoService.getAllBeans());

		mav.setViewName("country.index");

		return mav;
	}

	@RequestMapping(value = "/insert", method = RequestMethod.GET)
	public ModelAndView insertView(ModelAndView mav) {
		mav.addObject("countryBean", new CountryBean());

		mav.setViewName("country.form");

		return mav;
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public String insertAction(@ModelAttribute CountryBean countryBean) {
		Date now = new Date();

		System.out.println(countryBean.toString());

		Country country = new Country();
		country.setFullName(countryBean.getFullName());
		country.setShortName(countryBean.getShortName());
		country.setPopulation(countryBean.getPopulation());
		country.setCreatedDate(now);
		country.setModifiedDate(now);

		countryDaoService.insert(country);

		return "redirect:/country";
	}

	@RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
	public ModelAndView updateView(@PathVariable int id, ModelAndView mav) {
		CountryBean countryBean = countryDaoService.getBean(id);

		if (countryBean == null) {
			mav.setViewName("redirect:/country");

			return mav;
		}

		mav.addObject("countryBean", countryBean);

		mav.setViewName("country.form");

		return mav;
	}

	@RequestMapping(value = "/{id}/update", method = RequestMethod.POST)
	public String updateAction(@PathVariable int id, @ModelAttribute CountryBean countryBean) {
		Country country = countryDaoService.get(id);

		country.setFullName(countryBean.getFullName());
		country.setShortName(countryBean.getShortName());
		country.setPopulation(countryBean.getPopulation());
		country.setModifiedDate(new Date());

		countryDaoService.update(country);

		return "redirect:/country";
	}

	@RequestMapping(value = "/{id}/delete",method = RequestMethod.GET)
	public String deleteAction(@PathVariable int id) {
		countryDaoService.delete(countryDaoService.get(id));

		return "redirect:/country";
	}
}
