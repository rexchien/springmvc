package com.solvento.mvc.spring.dao.support.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import com.solvento.mvc.spring.dao.support.BasicCrud;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class BasicCrudImpl implements BasicCrud {
	private static final Log logger = LogFactory.getLog(BasicCrudImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;

	@Transactional
	public <T> void save(T entity) {
		logger.debug("saving " + entity.getClass().getSimpleName() + " instance");
		try {
			this.entityManager.persist(entity);
			logger.debug("save successful");
		} catch (RuntimeException e) {
			logger.error("save failed: " + e.getMessage(), e);
		}
	}

	@Transactional

	public <T> T update(T entity) {
		logger.debug("updating " + entity.getClass().getSimpleName() + " instance");
		try {
			T t = this.entityManager.merge(entity);
			logger.debug("update successful");
			return t;
		} catch (RuntimeException e) {
			logger.error("update failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@Transactional

	public <T> void delete(T entity) {
		if (entity != null) {
			Object obj = this.entityManager.merge(entity);
			if (obj == null) {
				return;
			}
			this.entityManager.remove(obj);
			this.entityManager.flush();
		}
	}


	public <T> T findById(Class<T> targetClass, Object id) {
		logger.debug("find " + targetClass.getSimpleName() + " instance with id: " + id);
		try {
			T instance = this.entityManager.find(targetClass, id);

			return instance;
		} catch (RuntimeException e) {
			logger.error("find failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")

	public <T> List<T> findAll(Class<T> targetClass) {
		logger.debug("find all " + targetClass.getClass());

		return entityManager.createQuery("from " + targetClass.getName()).getResultList();
	}


	public <T> int countAll(Class<T> targetClass) {
		return findAll(targetClass).size();
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

}
