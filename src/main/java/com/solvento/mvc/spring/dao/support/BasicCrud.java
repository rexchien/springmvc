package com.solvento.mvc.spring.dao.support;

import java.util.List;

public interface BasicCrud {
	<T> void save(T entity);

	<T> T update(T entity);

	<T> void delete(T entity);

	<T> T findById(Class<T> targetClass, Object id);

	<T> List<T> findAll(Class<T> targetClass);
	
	/**
	 * 取得資料表總筆數
	 * @return
	 */
	<T> int countAll(Class<T> targetClass);
}
