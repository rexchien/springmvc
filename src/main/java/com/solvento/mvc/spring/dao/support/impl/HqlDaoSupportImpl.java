package com.solvento.mvc.spring.dao.support.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import com.solvento.mvc.spring.dao.support.BaseJpaCallback;
import com.solvento.mvc.spring.dao.support.HqlDaoSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

@Repository
public class HqlDaoSupportImpl implements HqlDaoSupport {
	private static final Log logger = LogFactory.getLog(HqlDaoSupportImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;


	public void batchExecuteBySql(String hql) {
		try {
			Query query = entityManager.createQuery(hql);

			entityManager.getTransaction().begin();
			query.executeUpdate();
			entityManager.getTransaction().commit();
		} catch (RuntimeException e) {
			logger.error("batch execute by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}


	public int countAll(Class targetClass) {
		return countByHql("from " + targetClass.getSimpleName(), new Object[0]);
	}


	public int countByHql(String hql, Map<String, Object> paramMap) {
		List<Object> list = findByHql("select sum(1) " + hql, paramMap);
		if (list.size() > 0) {
			Object obj = list.get(0);
			if (obj != null) {
				return new Long(obj.toString()).intValue();
			}
		}
		return 0;
	}


	public int countByHql(String hql, Object[] params) {
		List<Object> list = findByHql("select sum(1) " + hql, params);
		if (list.size() > 0) {
			Object obj = list.get(0);
			if (obj != null) {
				return new Long(obj.toString()).intValue();
			}
		}
		return 0;
	}


	public List<Object> findByHql(String hql, Map<String, Object> paramMap) {
		return findByHql(hql, paramMap, Object.class);
	}


	public <T> List<T> findByHql(String hql, Map<String, Object> paramMap, Class<T> targetClass) {
		return findByHql(hql, paramMap, -1, -1, targetClass);
	}


	public List<Object> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit) {
		return findByHql(hql, paramMap, offset, limit, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> List<T> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit,
								 Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, paramMap, offset, limit)
					.doInJpa(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}


	public List<Object> findByHql(String hql, Object[] params) {
		return findByHql(hql, params, -1, -1, Object.class);
	}


	public <T> List<T> findByHql(String hql, Object[] params, Class<T> targetClass) {
		return findByHql(hql, params, -1, -1, targetClass);
	}


	public List<Object> findByHql(String hql, Object[] params, int offset, int limit) {
		return findByHql(hql, params, offset, limit, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> List<T> findByHql(String hql, Object[] params, int offset, int limit,
								 Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, params, offset, limit).doInJpa(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}


	public <T> List<T> findByProperty(String propertyName, Object value, Class<T> targetClass) {
		String hql = "from " + targetClass.getSimpleName() + " where " + propertyName + " = ?";
		Object[] params = new Object[]{value};
		return findByHql(hql, params, targetClass);
	}


	public List<Object> findByStoredProcedure(String hql, Map<String, Object> paramMap) {
		return findByStoredProcedure(hql, paramMap, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> List<T> findByStoredProcedure(String hql, Map<String, Object> paramMap,
											 Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, paramMap, -1, -1)
					.doInJpaNativeQuery(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}


	public List<Object> findByStoredProcedure(String hql, Object[] params) {
		return findByStoredProcedure(hql, params, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> List<T> findByStoredProcedure(String hql, Object[] params, Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, params, -1, -1)
					.doInJpaNativeQuery(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}


	public Optional<Object> findSingleByHql(String hql, Map<String, Object> paramMap) {
		return findSingleByHql(hql, paramMap, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> Optional<T> findSingleByHql(String hql, Map<String, Object> paramMap,
										   Class<T> targetClass) {
		Query query = entityManager.createQuery(hql);
		if (paramMap != null && paramMap.size() > 0) {
			for (String key : paramMap.keySet()) {
				query.setParameter(key, paramMap.get(key));
			}
		}
		query.setMaxResults(1);

		Optional<T> object = Optional.absent();
		try {
			object = Optional.of((T) query.getSingleResult());
		} catch (Exception nre) {
		}
		return object;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}


	public List<Object> findByNativeSql(String sql, int offset, int limit) {
		return findByNativeSql(sql, offset, limit, Object.class);
	}


	public <T> List<T> findByNativeSql(String sql, int offset, int limit, Class<T> targetClass) {
		return entityManager.createNativeQuery(sql, targetClass).getResultList();
	}


	public EntityManager getEntityManager() {
		return entityManager;
	}


	public <T> T findSingleByNativeSql(String sql, Map<String, Object> paramMap,
									   Class<T> targetClass) {
		Query query = entityManager.createNativeQuery(sql);

		for (String key : paramMap.keySet()) {
			query.setParameter(key, paramMap.get(key));
		}

		return (T) query.getSingleResult();
	}


	public Optional<Object> findSingleByProperty(String propertyName, Object value) {
		return findSingleByProperty(propertyName, value, Object.class);
	}

	@SuppressWarnings("unchecked")

	public <T> Optional<T> findSingleByProperty(String propertyName, Object value,
												Class<T> targetClass) {
		String hql = "from " + targetClass.getSimpleName() + " where " + propertyName + " = ?1";

		Query query = entityManager.createQuery(hql);
		query.setParameter(1, value);
		query.setMaxResults(1);

		Optional<T> object = Optional.absent();
		try {
			object = Optional.of((T) query.getSingleResult());
		} catch (Exception nre) {
		}
		return object;
	}
}