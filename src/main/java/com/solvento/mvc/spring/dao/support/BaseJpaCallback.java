package com.solvento.mvc.spring.dao.support;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

public class BaseJpaCallback {

	private String hql;
	private Object[] params;
	private Map<String, ?> paramMap;
	private int offset;
	private int limit;

	/**
	 * @param hql
	 * @param paramMap
	 * @param offset
	 * @param limit
	 */
	public BaseJpaCallback(String hql, Map<String, ?> paramMap, int offset, int limit) {
		super();
		this.hql = hql;
		this.paramMap = paramMap;
		this.offset = offset;
		this.limit = limit;
	}

	/**
	 * @param hql
	 * @param params
	 * @param offset
	 * @param limit
	 */
	public BaseJpaCallback(String hql, Object[] params, int offset, int limit) {
		super();
		this.hql = hql;
		this.params = params;
		this.offset = offset;
		this.limit = limit;
	}

	@SuppressWarnings("unchecked")
	public List<Object> doInJpa(EntityManager entityManager) throws PersistenceException {
		Query query = entityManager.createQuery(hql);
		if (paramMap != null && paramMap.size() > 0) {
			for (String key : paramMap.keySet()) {
				query.setParameter(key, paramMap.get(key));
			}
		} else if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				query.setParameter(i + 1, params[i]);
			}
		}

		if (offset > -1) {
			query.setFirstResult(offset);
		}
		if (limit > -1) {
			query.setMaxResults(limit);
		}

		return query.getResultList();
	}

	public List<?> doInJpaNativeQuery(EntityManager entityManager) throws PersistenceException {
		Query query = entityManager.createNativeQuery(hql);

		if (paramMap != null && paramMap.size() > 0) {
			for (String key : paramMap.keySet()) {
				query.setParameter(key, paramMap.get(key));
			}
		} else if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				query.setParameter(i + 1, params[i]);
			}
		}
		
		if (offset > -1) {
			query.setFirstResult(offset);
		}
		if (limit > -1) {
			query.setMaxResults(limit);
		}

		return query.getResultList();
	}
}
