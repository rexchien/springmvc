package com.solvento.mvc.spring.weight.annotation;

public enum WeightClass {
	DEFAULT, LIGHT, MIDDLE, HEAVY
}
