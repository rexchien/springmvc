package com.solvento.mvc.spring.weight;

import com.solvento.mvc.spring.weight.annotation.Weight;
import com.solvento.mvc.spring.weight.annotation.WeightClass;

import java.lang.reflect.Field;

public class WeightFactory {
	@SuppressWarnings({ "rawtypes" })
	public static Object parseWeightObj(Object obj, WeightClass targetWeight) {
		Class objClass = obj.getClass();
		Field[] fields = objClass.getDeclaredFields();
		
		int targetWeightOrdinal = targetWeight.ordinal();

		for (Field field : fields) {
			Weight weightAnnotation = field.getAnnotation(Weight.class);
			WeightClass weightClass = WeightClass.DEFAULT;

			if (weightAnnotation != null) {
				weightClass = weightAnnotation.value();
			}
			
			if (weightClass.ordinal() > targetWeightOrdinal) {
				field.setAccessible(true);
				try {
					field.set(obj, null);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		return obj;
	}
}
