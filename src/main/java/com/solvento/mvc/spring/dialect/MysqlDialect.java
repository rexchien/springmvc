package com.solvento.mvc.spring.dialect;

import org.hibernate.dialect.MySQL5Dialect;

/**
 * <p>Title: MysqlDialect</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 下午5:24</p>
 *
 * @author Rex Chien
 * @version 1.0
 */
public class MysqlDialect extends MySQL5Dialect {
}
