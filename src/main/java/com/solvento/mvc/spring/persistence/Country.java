package com.solvento.mvc.spring.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>Title: Country</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 下午4:46</p>
 * <p>
 *     CREATE TABLE `country` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fullName` varchar(45) NOT NULL,
 `shortName` varchar(10) NOT NULL,
 `population` int(11) NOT NULL,
 `createdDate` datetime NOT NULL,
 `modifiedDate` datetime NOT NULL,
 PRIMARY KEY (`id`)
 )
 * </p>
 *
 * @author Rex Chien
 * @version 1.0
 */
@Entity
public class Country {
	private int id;
	private String fullName;
	private String shortName;
	private int population;
	private Date createdDate;
	private Date modifiedDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "fullName", nullable = false, length = 45)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Basic
	@Column(name = "shortName", nullable = false, length = 10)
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Basic
	@Column(name = "population", nullable = false)
	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdDate", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedDate", nullable = false)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Country country = (Country) o;

		if (id != country.id) return false;
		if (population != country.population) return false;
		if (fullName != null ? !fullName.equals(country.fullName) : country.fullName != null) return false;
		if (shortName != null ? !shortName.equals(country.shortName) : country.shortName != null) return false;
		if (createdDate != null ? !createdDate.equals(country.createdDate) : country.createdDate != null) return false;
		if (modifiedDate != null ? !modifiedDate.equals(country.modifiedDate) : country.modifiedDate != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
		result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
		result = 31 * result + population;
		result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
		result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
		return result;
	}
}
