package com.solvento.mvc.spring.http;

import com.solvento.mvc.spring.utils.XssUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XssEscapedHttpServletRequest extends HttpServletRequestWrapper {

	public XssEscapedHttpServletRequest(HttpServletRequest request) {
		super(request);
	}

	public String getParameter(String name) {
		return XssUtils.clean(getRequest().getParameter(name));
	}

	public String getRawParameter(String name) {
		return getRequest().getParameter(name);
	}

	public Map<String, String[]> getParameterMap() {
		Map<String, String[]> escapedMap = new HashMap<String, String[]>();

		Map<String, String[]> map = getRequest().getParameterMap();
		Iterator<String> iter = map.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			String[] values = map.get(key);

			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					if (values[i] == null || values[i].length() == 0) {
						continue;
					}
					values[i] = XssUtils.clean(values[i]);
				}
			}

			escapedMap.put(key, values);
		}

		return escapedMap;
	}
}
