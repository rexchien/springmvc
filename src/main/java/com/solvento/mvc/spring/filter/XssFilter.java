package com.solvento.mvc.spring.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solvento.mvc.spring.http.XssEscapedHttpServletRequest;
import org.springframework.web.filter.OncePerRequestFilter;

@WebFilter(filterName = "xssFilter", urlPatterns = {"/*"})
public class XssFilter extends OncePerRequestFilter {
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
									FilterChain filterChain) throws ServletException, IOException {
		filterChain.doFilter(new XssEscapedHttpServletRequest(request), response);
	}

}
