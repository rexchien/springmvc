package com.solvento.mvc.spring.interceptor;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public abstract class AbstractAuthorizationInterceptor extends HandlerInterceptorAdapter {
	
	private Pattern[] uriRegexPatternList;

	public void setUriRegexPatternList(Pattern[] uriRegexPatternList) {
		this.uriRegexPatternList = uriRegexPatternList;
	}

	protected abstract boolean checkValidation();

	protected abstract void doValidateFailed(HttpServletRequest request, HttpServletResponse response);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		String path = request.getRequestURI();

		if (included(path)) {
			if (!checkValidation()) {
				doValidateFailed(request, response);
				return false;
			}
		}
		
		return true;
	}
	
	private boolean included(String uri) {
		for (Pattern p : uriRegexPatternList) {
			if (p.matcher(uri).find()) {
				return true;
			}
		}
		return false;
	}

}
