package com.solvento.mvc.spring.interceptor.impl;

import com.solvento.mvc.spring.interceptor.AbstractAuthorizationInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * <p>Title: BasicRequiredLoginInterceptor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/14 下午1:57</p>
 *
 * @author Rex Chien
 * @version 1.0
 */
public class BasicRequiredLoginInterceptor extends AbstractAuthorizationInterceptor {
	@Override
	protected boolean checkValidation() {
		return false;
	}

	@Override
	protected void doValidateFailed(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.sendRedirect("/country");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
