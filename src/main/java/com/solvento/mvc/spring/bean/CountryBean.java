package com.solvento.mvc.spring.bean;

import org.springframework.web.servlet.view.tiles3.TilesConfigurer;

/**
 * <p>Title: CountryBean</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 下午4:53</p>
 *
 * @author Rex Chien
 * @version 1.0
 */
public class CountryBean {
	private int id;
	private String fullName;
	private String shortName;
	private int population;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	@Override
	public String toString() {
		return "CountryBean{" +
				"id=" + id +
				", fullName='" + fullName + '\'' +
				", shortName='" + shortName + '\'' +
				", population=" + population +
				'}';
	}
}
