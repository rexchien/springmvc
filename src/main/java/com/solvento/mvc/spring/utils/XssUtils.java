package com.solvento.mvc.spring.utils;

import org.apache.commons.lang3.StringUtils;

public class XssUtils {
	/**
	 * 清除字串中有可能發生XSS攻擊的pattern
	 * 
	 * @param str
	 *            欲檢查的字串
	 * @return
	 */
	public static String clean(String str) {
		if (StringUtils.isEmpty(str)) {
			return str;
		}

		return str.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\\(", "&#40;")
				.replaceAll("\\)", "&#41;").replaceAll("'", "&#39;")
				.replaceAll("eval\\((.*)\\)", "")
				.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"")
				.replaceAll("script", "");
	}
}
