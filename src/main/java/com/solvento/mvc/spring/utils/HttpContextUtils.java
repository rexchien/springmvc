package com.solvento.mvc.spring.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class HttpContextUtils {
	private static final String RESPONSE_NAME_AT_ATTRIBUTES = ServletRequestAttributes.class
			.getName() + ".ATTRIBUTE_NAME";

	public static String getServerPath() {
		HttpServletRequest request = getRequest();
		
		return request.getScheme() + "://" + request.getHeader("host") + request.getContextPath();
	}

	public static HttpServletRequest getRequest() {
		return getRequestAttributes().getRequest();
	}
	
	public static String getUserAgent() {
		return getRequest().getHeader("user-agent");
	}
	
	public static String getReferrer() {
		return getRequest().getHeader("Referer");
	}

	public static HttpServletResponse getResponse() {
		return (HttpServletResponse) getRequestAttributes().getAttribute(
				RESPONSE_NAME_AT_ATTRIBUTES, RequestAttributes.SCOPE_REQUEST);
	}

	public static void setResponse(HttpServletResponse response) {
		getRequestAttributes().setAttribute(RESPONSE_NAME_AT_ATTRIBUTES, response,
				RequestAttributes.SCOPE_REQUEST);
	}

	public static HttpSession getSession() {
		return getRequest().getSession();
	}

	private static ServletRequestAttributes getRequestAttributes() {
		return (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	}

	public static void addCookie(String cookieName, String cookieValue, int maxAge)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(cookieName, URLEncoder.encode(cookieValue, "utf-8"));

		cookie.setPath("/");
		cookie.setMaxAge(maxAge);
		
		getResponse().addCookie(cookie);
	}

	public static String getCookieValue(String cookieName) {
		String cookieValue = null;
		Cookie[] cookies = getRequest().getCookies();
		if (cookies != null) {
			int i = 0;
			boolean cookieExists = false;
			while (!cookieExists && i < cookies.length) {
				if (cookies[i].getName().equals(cookieName)) {
					cookieExists = true;
					cookieValue = cookies[i].getValue();
				} else {
					i++;
				}
			}
		}
		return cookieValue;
	}
}
