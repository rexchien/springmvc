package com.solvento.mvc.spring.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import com.solvento.mvc.spring.dao.support.BasicCrud;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BasicCrudServiceImpl<T> {
	private static final Log logger = LogFactory.getLog(BasicCrudServiceImpl.class);

	@Resource
	private BasicCrud basicCrud;

	public void delete(T entity) {
		basicCrud.delete(entity);
		logger.debug(String.format("%s delete successfully, info = %s", getGenericSimpleName(), entity));
	}

	public T get(Object id) {
		return basicCrud.findById(getGenericType(), id);
	}

	public List<T> findAll() {
		return basicCrud.findAll(getGenericType());
	}

	public boolean insert(T entity) {
		if (entity != null) {
			basicCrud.save(entity);
			logger.debug(String.format("%s insert successfully, info = %s", getGenericSimpleName(), entity));
			return true;
		} else {
			return false;
		}
	}

	public void update(T entity) {
		basicCrud.update(entity);
		logger.debug(String.format("%s update successfully, info = %s", getGenericSimpleName(), entity));
	}

	@SuppressWarnings("unchecked")
	private Class<T> getGenericType() {
		return (Class<T>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	private String getGenericSimpleName() {
		return getGenericType().getSimpleName();
	}

}
