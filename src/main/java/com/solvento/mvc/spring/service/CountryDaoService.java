package com.solvento.mvc.spring.service;

import com.solvento.mvc.spring.bean.CountryBean;
import com.solvento.mvc.spring.persistence.Country;

import java.util.List;

/**
 * <p>Title: CountryDaoService</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 下午3:37</p>
 *
 * @author Rex Chien
 * @version 1.0
 */
public interface CountryDaoService extends BasicCrudService<Country> {
	CountryBean getBean(int id);

	List<CountryBean> getAllBeans();
}
