package com.solvento.mvc.spring.service;

import java.util.List;

public interface BasicCrudService<T> {
	boolean insert(T entity);

	void update(T entity);

	void delete(T entity);

	T get(Object id);

	List<T> findAll();
	
}
