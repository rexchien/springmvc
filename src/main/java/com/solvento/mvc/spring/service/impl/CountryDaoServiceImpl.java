package com.solvento.mvc.spring.service.impl;

import com.solvento.mvc.spring.bean.CountryBean;
import com.solvento.mvc.spring.persistence.Country;
import com.solvento.mvc.spring.service.BasicCrudService;
import com.solvento.mvc.spring.service.CountryDaoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>Title: CountryDaoServiceImpl</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Solvento Soft</p>
 * <p>Created Date: 2016/6/8 下午3:42</p>
 *
 * @author Rex Chien
 * @version 1.0
 */

@Service
public class CountryDaoServiceImpl extends BasicCrudServiceImpl<Country> implements CountryDaoService {
	private static final Log logger = LogFactory.getLog(CountryDaoServiceImpl.class);

	public CountryBean getBean(int id) {
		Country country = get(id);
		if (country == null) {
			return null;
		}

		return persistenceToBean(country);
	}

	public List<CountryBean> getAllBeans() {
		List<Country> countries = findAll();

		return countries.stream().map(country -> persistenceToBean(country)).collect(Collectors.toList());
	}

	private CountryBean persistenceToBean(Country country) {
		CountryBean bean = new CountryBean();

		bean.setId(country.getId());
		bean.setFullName(country.getFullName());
		bean.setShortName(country.getShortName());
		bean.setPopulation(country.getPopulation());

		return bean;
	}
}
