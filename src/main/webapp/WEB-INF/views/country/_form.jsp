<%--
  Created by IntelliJ IDEA.
  User: rex.chien
  Date: 2016/6/8
  Time: 下午5:48
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../tag-libs.jsp" %>

<form method="post">
	<div class="form-group">
		<label for="fullName">Full Name</label>
		<input type="text" name="fullName" id="fullName" size="45" value="${countryBean.fullName}" class="form-control"/>
	</div>
	<div class="form-group">
		<label for="shortName">Short Name</label>
		<input type="text" name="shortName" id="shortName" size="10" value="${countryBean.shortName}" class="form-control"/>
	</div>
	<div class="form-group">
		<label for="population">Population</label>
		<input type="number" name="population" id="population" value="${countryBean.population}" class="form-control"/>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>