<%--
  Created by IntelliJ IDEA.
  User: rex.chien
  Date: 2016/6/8
  Time: 下午5:05
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../tag-libs.jsp" %>

<div class="page-header">
	<h1><spring:message code="country.header.index"/></h1>
	<a class="btn btn-default" href="${contextPath}/country/insert"><spring:message
			code="function.insert"/></a>
</div>

<table class="table">
	<tr>
		<th><spring:message code="country.fullname"/></th>
		<th><spring:message code="country.shortname"/></th>
		<th><spring:message code="country.population"/></th>
		<th></th>
	</tr>
	<c:forEach items="${countries}" var="country">
		<tr>
			<td>${country.fullName}</td>
			<td>${country.shortName}</td>
			<td>${country.population}</td>
			<td>
				<a href="${contextPath}/country/${country.id}/update">
					<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
				</a>
				<a href="${contextPath}/country/${country.id}/delete">
					<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
				</a>
			</td>
		</tr>
	</c:forEach>
</table>
