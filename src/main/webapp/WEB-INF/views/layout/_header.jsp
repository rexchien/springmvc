<%--
  Created by IntelliJ IDEA.
  User: rex.chien
  Date: 2016/6/8
  Time: 下午2:25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../tag-libs.jsp" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="${contextPath}">Spring MVC Sample</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?lang=zh_TW">繁中</a></li>
				<li><a href="?lang=zh_CN">简中</a></li>
				<li><a href="?lang=en_US">ENG</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>
