<%--
  Created by IntelliJ IDEA.
  User: rex.chien
  Date: 2016/6/8
  Time: 下午2:22
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../tag-libs.jsp" %>

<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Spring MVC Sample</title>

	<link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${contextPath}/css/font-awesome.min.css" rel="stylesheet">
	<link href="${contextPath}/css/style.css" rel="stylesheet" type="text/css">

	<script src="${contextPath}/js/jquery-2.2.4.min.js"></script>
</head>
<body>
<tiles:insertAttribute name="header"/>
<div class="container content">
<tiles:insertAttribute name="content"/>
</div>
<tiles:insertAttribute name="footer"/>
</body>

<script src="${contextPath}/js/bootstrap.min.js"></script>

</html>
